package renan.challange.mobilepayment;

import com.google.inject.AbstractModule;

import renan.challange.mobilepayment.domain.bo.MainBO;
import renan.challange.mobilepayment.domain.bo.PaymentsBO;
import renan.challange.mobilepayment.domain.bo.SignInBO;
import renan.challange.mobilepayment.domain.bo.SignUpBO;
import renan.challange.mobilepayment.domain.bo.StoreBO;
import renan.challange.mobilepayment.domain.bo.impl.MainBOImpl;
import renan.challange.mobilepayment.domain.bo.impl.PaymentsBOImpl;
import renan.challange.mobilepayment.domain.bo.impl.SignInBOImpl;
import renan.challange.mobilepayment.domain.bo.impl.SignUpBOImpl;
import renan.challange.mobilepayment.domain.bo.impl.StoreBOImpl;
import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import renan.challange.mobilepayment.presenter.SignInPresenter;
import renan.challange.mobilepayment.presenter.SignUpPresenter;
import renan.challange.mobilepayment.presenter.StorePresenter;
import renan.challange.mobilepayment.presenter.MainPresenter;
import renan.challange.mobilepayment.presenter.impl.MainPresenterImpl;
import renan.challange.mobilepayment.presenter.impl.PaymentsPresenterImpl;
import renan.challange.mobilepayment.presenter.impl.SignInPresenterImpl;
import renan.challange.mobilepayment.presenter.impl.SignUpPresenterImpl;
import renan.challange.mobilepayment.presenter.impl.StorePresenterImpl;

/**
 * Created by RenanKirk on 03/02/2017.
 */

public class BindingModule extends AbstractModule {

    @Override
    protected void configure() {
        //presenter
        bind(SignInPresenter.class).to(SignInPresenterImpl.class);
        bind(SignUpPresenter.class).to(SignUpPresenterImpl.class);
        bind(StorePresenter.class).to(StorePresenterImpl.class);
        bind(PaymentsPresenter.class).to(PaymentsPresenterImpl.class);
        bind(MainPresenter.class).to(MainPresenterImpl.class);

        //bo
        bind(SignInBO.class).to(SignInBOImpl.class);
        bind(SignUpBO.class).to(SignUpBOImpl.class);
        bind(StoreBO.class).to(StoreBOImpl.class);
        bind(PaymentsBO.class).to(PaymentsBOImpl.class);
        bind(MainBO.class).to(MainBOImpl.class);
    }
}
