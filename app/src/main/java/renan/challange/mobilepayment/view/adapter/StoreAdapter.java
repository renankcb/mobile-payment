package renan.challange.mobilepayment.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.StorePresenter;
import renan.challange.mobilepayment.view.StoreView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreViewHolder> {

    private final List<StoreItem> items;
    private final StoreView view;

    public StoreAdapter(List<StoreItem> items, StoreView view){
        this.items = items;
        this.view = view;
    }

    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_item, null);
        StoreViewHolder rcv = new StoreViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(StoreViewHolder holder, final int position) {
        holder.name.setText(items.get(position).getName());
        holder.price.setText("R$" + String.valueOf(items.get(position).getPrice()));
        holder.price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.buyItem(items.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateData(List<StoreItem> allStoreItems) {
        items.clear();
        items.addAll(allStoreItems);
        notifyDataSetChanged();
    }

    public class StoreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView name;
        private final TextView price;

        public StoreViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name = (TextView) itemView.findViewById(R.id.txv_store_item_name);
            price = (TextView) itemView.findViewById(R.id.txv_store_item_price);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
