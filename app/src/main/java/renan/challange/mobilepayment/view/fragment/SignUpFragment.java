package renan.challange.mobilepayment.view.fragment;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.inject.Inject;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.presenter.SignUpPresenter;
import renan.challange.mobilepayment.view.LoginView;
import renan.challange.mobilepayment.view.Utils.ViewUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends RoboFragment implements LoginView {

    public static final String SIGNUP_TAB_TITLE = "Sign Up";
    @InjectView(R.id.input_layout_su_username)
    private TextInputLayout layoutUsername;

    @InjectView(R.id.input_username)
    private EditText userName;

    @InjectView(R.id.input_layout_first_name)
    private TextInputLayout layoutFirstName;

    @InjectView(R.id.input_first_name)
    private EditText firstName;

    @InjectView(R.id.input_layout_last_name)
    private TextInputLayout layoutLastName;

    @InjectView(R.id.input_last_name)
    private EditText lastName;

    @InjectView(R.id.input_layout_email)
    private TextInputLayout layoutEmail;

    @InjectView(R.id.input_email)
    private EditText email;

    @InjectView(R.id.input_layout_su_password)
    private TextInputLayout layoutPassword;

    @InjectView(R.id.input_su_password)
    private EditText password;

    @InjectView(R.id.input_layout_phone)
    private TextInputLayout layoutPhone;

    @InjectView(R.id.input_phone)
    private EditText phone;

    @InjectView(R.id.btn_sign_up)
    private Button signUp;

    @Inject
    private SignUpPresenter signupPresenter;

    @Inject
    private ViewUtils viewUtils;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        signupPresenter.setView(this);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
    }

    /**
     * Validating form
     */
    private void submitForm() {
        String name = userName.getText().toString().trim();
        String fName = firstName.getText().toString().trim();
        String lName = lastName.getText().toString().trim();
        String mail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String pNumber = phone.getText().toString().trim();

        if (!viewUtils.IsValidField(
                name,
                layoutUsername,
                userName,
                getString(R.string.err_msg_name))) {
            return;
        }

        if (!viewUtils.IsValidField(
                fName,
                layoutFirstName,
                firstName,
                getString(R.string.err_msg_first_name))) {
            return;
        }

        if (!viewUtils.IsValidField(
                lName,
                layoutLastName,
                lastName,
                getString(R.string.err_msg_first_name))) {
            return;
        }

        if (!viewUtils.IsValidField(
                mail,
                layoutEmail,
                email,
                getString(R.string.err_msg_email))) {
            return;
        }

        if (!viewUtils.IsValidField(
                pass,
                layoutPassword,
                password,
                getString(R.string.err_msg_password))) {
            return;
        }

        if(!viewUtils.isNetworkAvailable()){
            viewUtils.showToast(getString(R.string.fail_connection));
            return;
        }

        viewUtils.showProgressDialog();
        signupPresenter.signUup(name, fName, lName, mail, pass, pNumber);
    }


    @Override
    public void doSuccess() {
        viewUtils.hideProgressDialog();
        userName.setText("");
        firstName.setText("");
        lastName.setText("");
        email.setText("");
        password.setText("");
        phone.setText("");
        viewUtils.showToast("Usuário registrado com sucesso");
    }

    @Override
    public void doFail(String message) {
        viewUtils.showToast(message);
        viewUtils.hideProgressDialog();
    }
}
