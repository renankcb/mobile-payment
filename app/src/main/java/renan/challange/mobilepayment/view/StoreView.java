package renan.challange.mobilepayment.view;

import renan.challange.mobilepayment.domain.model.StoreItem;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface StoreView {
    void failOnCreteDAO(String message);

    void buyItem(StoreItem item);

    void buyItemSuccess();

    void buyItemFail(String message);
}
