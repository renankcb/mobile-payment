package renan.challange.mobilepayment.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import renan.challange.mobilepayment.view.PaymentsView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.PaymentsViewHolder> {

    private final List<PaymentRequest> items;
    private final PaymentsView view;

    public PaymentsAdapter(List<PaymentRequest> items, PaymentsView view) {
        this.items = items;
        this.view = view;
    }

    @Override
    public PaymentsAdapter.PaymentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_item, parent, false);
        PaymentsAdapter.PaymentsViewHolder rcv = new PaymentsAdapter.PaymentsViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(PaymentsViewHolder holder, final int position) {
        holder.paymentName.setText(items.get(position).getName());
        holder.paymentDescription.setText(items.get(position).getDescription());
        holder.senderName.setText(items.get(position).getSender().getName());
        holder.paymentDate.setText(items.get(position).getDate());
        holder.menuPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.showOptioins(v, items.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void removeItem(PaymentRequest itemSelected) {
       notifyItemRemoved(0);
    }

    public class PaymentsViewHolder extends RecyclerView.ViewHolder{
        private final TextView paymentName;
        private final TextView paymentDescription;
        private final TextView senderName;
        private final TextView paymentDate;
        private final ImageView menuPayment;

        public PaymentsViewHolder(View itemView) {
            super(itemView);
            paymentName = (TextView) itemView.findViewById(R.id.txv_payment_name);
            paymentDescription = (TextView) itemView.findViewById(R.id.txv_payment_description);
            senderName = (TextView) itemView.findViewById(R.id.txv_payment_sender_name);
            paymentDate = (TextView) itemView.findViewById(R.id.txv_payment_date);
            menuPayment = (ImageView) itemView.findViewById(R.id.btn_menu_payment);
        }
    }
}
