package renan.challange.mobilepayment.view;

import renan.challange.mobilepayment.domain.model.User;

/**
 * Created by RenanKirk on 05/02/2017.
 */
public interface MainView {
    void getUserSuccess(User user);

    void getUserFail(String message);

    void logoutSuccess();

    void logoutFail();
}
