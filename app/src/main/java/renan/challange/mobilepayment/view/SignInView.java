package renan.challange.mobilepayment.view;

/**
 * Created by RenanKirk on 07/02/2017.
 */

public interface SignInView extends LoginView {

    void isLoggedIn();
}
