package renan.challange.mobilepayment.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import renan.challange.mobilepayment.R;
import roboguice.fragment.RoboFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends RoboFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

}
