package renan.challange.mobilepayment.view.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.view.fragment.SignInFragment;
import renan.challange.mobilepayment.view.fragment.SignUpFragment;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

public class LoginActivity extends RoboActionBarActivity {

    /**
     * ViewPager view to control tabs
     */
    @InjectView(R.id.viewpager_login)
    private ViewPager viewPager;

    /**
     * TabLayout view to show tabs
     */
    @InjectView(R.id.tabs_login)
    private TabLayout tabs;

    /**
     * Sign In Fragment to show in Sign In Tab
     */
    @Inject
    private SignInFragment signInFragment;

    /**
     * Sign In Fragment to show in Sign In Tab
     */
    @Inject
    private SignUpFragment signUpFragment;

    /**
     * View pager adapter of signin and signup frament
     */
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupViewPager(viewPager);
        tabs.setupWithViewPager(viewPager);
    }

    /**
     * Crate pager adapter and add fragments
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(signInFragment, SignInFragment.SIGNIN_TAB_TITLE);
        adapter.addFragment(signUpFragment, SignUpFragment.SIGNUP_TAB_TITLE);
        viewPager.setAdapter(adapter);
    }

    /**
     * FragmentPagerAdapter
     */
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public ViewPagerAdapter getAdapter(){
        return adapter;
    }

}
