package renan.challange.mobilepayment.view.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import renan.challange.mobilepayment.R;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class ViewUtils {

    private final Activity context;

    private Dialog progressDialog;

    @Inject
    public ViewUtils(Activity context){
        this.context = context;
    }

    /**
     * Valid fields from login
     * @param input
     * @param layoutInput
     * @param editInput
     * @param errorMsg
     * @return
     */
    public boolean IsValidField(String input,
                                       TextInputLayout layoutInput,
                                       EditText editInput,
                                       String errorMsg) {
        if (isValid(input)) {
            layoutInput.setErrorEnabled(false);
            return true;
        } else {
            layoutInput.setError(errorMsg);
            requestFocus(editInput);
            return false;
        }
    }

    public boolean isValid(String field){
        if(field.isEmpty()){
            return false;
        }
        return true;
    }

    /**
     * Set focus to view
     * @param view
     */
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * show message in toast
     * @param msg
     */
    public void showToast(String msg) {
        Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * create and show dialog
     */
    public void showProgressDialog() {
        progressDialog = new Dialog(context, R.style.full_screen_dialog);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.show();
    }

    /**
     * dismiss dialog
     */
    public void hideProgressDialog(){
        progressDialog.dismiss();
        progressDialog = null;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
