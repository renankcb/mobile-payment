package renan.challange.mobilepayment.view.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.domain.model.Sender;
import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import renan.challange.mobilepayment.view.PaymentsView;
import renan.challange.mobilepayment.view.Utils.ViewUtils;
import renan.challange.mobilepayment.view.adapter.PaymentsAdapter;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentsListFragment extends RoboFragment implements PaymentsView {

    @InjectView(R.id.rv_payments)
    private RecyclerView payments;

    @Inject
    private PaymentsPresenter presenter;

    @Inject
    private ViewUtils viewUtils;

    private PaymentsAdapter adapter;

    private List<PaymentRequest> rowListItem = new ArrayList<>();

    private AlertDialog.Builder alertDialogBuilder;

    //used to simulate delete from list
    private PaymentRequest itemSelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payments_list, container, false);
    }

    private void setupViewAndAdapter() {
        //TODO remover
        rowListItem = mockItems();

        adapter = new PaymentsAdapter(rowListItem, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        payments.setHasFixedSize(true);
        payments.setLayoutManager(mLayoutManager);
        payments.setAdapter(adapter);

        alertDialogBuilder = new AlertDialog.Builder(getActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        setupViewAndAdapter();
    }

    /**
     * simulate payments done
     * @return
     */
    private List<PaymentRequest> mockItems(){
        List<PaymentRequest> items = new ArrayList<>();
        Sender sender = new Sender("kirk", "renankcb@gmail.com");
        PaymentRequest item = new PaymentRequest(sender,"TESTE TESTE", "Teste teste teste teste", "aeiou", "05/02/2017");
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        items.add(item);
        return items;
    }

    @Override
    public void showOptioins(View v, PaymentRequest item) {
        showPopupMenu(v, item);
    }

    @Override
    public void deletePaymentFail(String message) {
        viewUtils.hideProgressDialog();
        viewUtils.showToast(message);
    }

    /**
     * simulate teleted payment
     */
    @Override
    public void deletePaymentSuccess() {
        viewUtils.hideProgressDialog();
        adapter.removeItem(itemSelected);
        viewUtils.showToast(getString(R.string.payment_deleted));
    }

    private void showPopupMenu(View view, final PaymentRequest item) {
        PopupMenu popup = new PopupMenu(view.getContext(),view );
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_payment_item, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.menu_delet:
                        alertDialogBuilder.setMessage(getString(R.string.delete_item));
                        alertDialogBuilder.setPositiveButton(getString(R.string.delete), getPositiveAction(item));
                        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), getNegativeAction());
                        alertDialogBuilder.show();
                        break;
                }
                return false;
            }

            private DialogInterface.OnClickListener getPositiveAction(final PaymentRequest item){
                return new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!viewUtils.isNetworkAvailable()){
                            viewUtils.showToast(getString(R.string.fail_connection));
                            return;
                        }
                        presenter.deleteItem(item.getId());
                        viewUtils.showProgressDialog();
                    }
                };
            }

            private DialogInterface.OnClickListener getNegativeAction(){
                return new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //empty method
                    }
                };
            }
        });
        popup.show();
    }
}
