package renan.challange.mobilepayment.view.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.StorePresenter;
import renan.challange.mobilepayment.view.StoreView;
import renan.challange.mobilepayment.view.Utils.ViewUtils;
import renan.challange.mobilepayment.view.adapter.StoreAdapter;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreFragment extends RoboFragment implements StoreView {

    @InjectView(R.id.fab_add_item_store)
    private FloatingActionButton addItem;

    @InjectView(R.id.rv_store)
    private RecyclerView store;

    @Inject
    private StorePresenter presenter;

    @Inject
    private ViewUtils viewUtils;

    private StoreAdapter adapter;

    private List<StoreItem> rowListItem = new ArrayList<>();

    private GridLayoutManager layoutManager;

    private AlertDialog.Builder alertDialogBuilder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_store, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.setView(this);
        setupViewAndAdapter();

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    presenter.addCreateItemOnDB();
                    updateList();
                } catch (SQLException e) {
                    viewUtils.showToast(e.getMessage());
                }
            }
        });

        alertDialogBuilder = new AlertDialog.Builder(getActivity());
    }

    private void setupViewAndAdapter() {
        layoutManager = new GridLayoutManager(getActivity(), 2);
        adapter = new StoreAdapter(rowListItem, this);
        store.setHasFixedSize(true);
        store.setLayoutManager(layoutManager);
        store.setAdapter(adapter);
        updateList();
    }

    private void updateList() {
        try {
            adapter.updateData(presenter.getAllStoreItems());
        } catch (SQLException e) {
            viewUtils.showToast(e.getMessage());
        }
    }

    @Override
    public void failOnCreteDAO(String message) {
        viewUtils.showToast(message);
    }

    @Override
    public void buyItem(StoreItem item) {
        alertDialogBuilder.setMessage(getString(R.string.buy_item));
        alertDialogBuilder.setPositiveButton(getString(R.string.buy), getPositiveAction(item));
        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), getNegativeAction());
        alertDialogBuilder.show();
    }

    @Override
    public void buyItemSuccess() {
        viewUtils.hideProgressDialog();
        viewUtils.showToast(getString(R.string.payment_done));
    }

    @Override
    public void buyItemFail(String message) {
        viewUtils.hideProgressDialog();
        viewUtils.showToast(message);
    }

    private DialogInterface.OnClickListener getPositiveAction(final StoreItem item){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!viewUtils.isNetworkAvailable()){
                    viewUtils.showToast(getString(R.string.fail_connection));
                    return;
                }
                presenter.buyItem(item);
                viewUtils.showProgressDialog();
            }
        };
    }

    private DialogInterface.OnClickListener getNegativeAction(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //empty method
            }
        };
    }
}
