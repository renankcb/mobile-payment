package renan.challange.mobilepayment.view;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface LoginView {

    /**
     * Used when return success from BO
     */
    void doSuccess();

    /**
     * Used when return fail from BO
     */
    void doFail(String message);
}
