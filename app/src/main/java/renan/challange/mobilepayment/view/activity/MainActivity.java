package renan.challange.mobilepayment.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.inject.Inject;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.MainPresenter;
import renan.challange.mobilepayment.view.MainView;
import renan.challange.mobilepayment.view.Utils.ViewUtils;
import renan.challange.mobilepayment.view.fragment.LicenseFragment;
import renan.challange.mobilepayment.view.fragment.PaymentsListFragment;
import renan.challange.mobilepayment.view.fragment.StoreFragment;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

public class MainActivity extends RoboActionBarActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView {

    public static final String USER_NAME = "username_logged_in";
    public static final String IS_LOGGED_IN = "is_logged_in";
    @InjectView(R.id.drawer_layout)
    private DrawerLayout drawer;

    @InjectView(R.id.toolbar)
    private Toolbar toolbar;

    @InjectView(R.id.nav_view)
    private NavigationView navigationView;

    private TextView navUserName;

    private TextView navUserMail;

    @Inject
    private MainPresenter mainPresenter;

    private ActionBarDrawerToggle toggle;

    @Inject
    private ViewUtils viewUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter.setView(this);

        navUserMail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txv_nav_user_loggedin);
        navUserName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txv_nav_user_loggedin);

        setupNavigationView();

        viewUtils.showProgressDialog();

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().get(IS_LOGGED_IN) == null) {
                mainPresenter.getUserLoggedIn(getIntent().getExtras().getString(USER_NAME));
            } else {
                mainPresenter.userLoggedIn();
            }
        }
    }

    private void setupNavigationView() {
        setSupportActionBar(toolbar);
        toggle = setupDrawerToggle();
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(getString(R.string.wah_logout));
            alertDialogBuilder.setPositiveButton(getString(R.string.action_logout), getPositiveAction());
            alertDialogBuilder.setNegativeButton(getString(R.string.cancel), getNegativeAction());
            alertDialogBuilder.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private DialogInterface.OnClickListener getPositiveAction() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!viewUtils.isNetworkAvailable()) {
                    viewUtils.showToast(getString(R.string.fail_connection));
                    return;
                }
                mainPresenter.logout();
                viewUtils.showProgressDialog();
            }
        };
    }

    private DialogInterface.OnClickListener getNegativeAction() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //empty method
            }
        };
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Class fragmentClass = null;
        boolean isAlert = false;
        int id = item.getItemId();

        if (id == R.id.nav_store) {
            fragmentClass = StoreFragment.class;
        } else if (id == R.id.nav_payments) {
            fragmentClass = PaymentsListFragment.class;
        } else if (id == R.id.nav_license) {
            fragmentClass = LicenseFragment.class;
        } else if (id == R.id.nav_about) {
            isAlert = true;
        }

        if (isAlert) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getString(R.string.about_text_title));
            alertDialogBuilder.setMessage(getString(R.string.about_text_msg));
            alertDialogBuilder.setNeutralButton(getString(R.string.ok), getNegativeAction());
            alertDialogBuilder.show();
        } else {
            openFragment(fragmentClass);
            item.setChecked(true);
            setTitle(item.getTitle());
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(Class fragmentClass) {
        try {
            Fragment fragment = (Fragment) fragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_layout, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getUserSuccess(User user) {
        navUserName.setText(user.getUsername());
        navUserMail.setText(user.getEmail());
        openFragment(StoreFragment.class);
        setTitle(getString(R.string.nav_store));
        viewUtils.hideProgressDialog();
    }

    @Override
    public void getUserFail(String message) {
        viewUtils.hideProgressDialog();
        viewUtils.showToast(message);
    }

    @Override
    public void logoutSuccess() {
        viewUtils.hideProgressDialog();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void logoutFail() {
        viewUtils.hideProgressDialog();
        viewUtils.showToast(getString(R.string.logout_fail));
    }
}
