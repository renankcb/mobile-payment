package renan.challange.mobilepayment.view;

import android.view.View;

import renan.challange.mobilepayment.domain.model.PaymentRequest;

/**
 * Created by RenanKirk on 04/02/2017.
 */
public interface PaymentsView {
    void showOptioins(View v, PaymentRequest item);

    void deletePaymentFail(String message);

    void deletePaymentSuccess();
}
