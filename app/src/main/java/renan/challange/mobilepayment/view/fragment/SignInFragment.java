package renan.challange.mobilepayment.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.inject.Inject;

import renan.challange.mobilepayment.R;
import renan.challange.mobilepayment.presenter.SignInPresenter;
import renan.challange.mobilepayment.view.LoginView;
import renan.challange.mobilepayment.view.SignInView;
import renan.challange.mobilepayment.view.Utils.ViewUtils;
import renan.challange.mobilepayment.view.activity.MainActivity;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends RoboFragment implements SignInView {

    public static final String SIGNIN_TAB_TITLE = "Sign In";
    private static final String IS_LOGGED_IN = "isLoggedIn";

    @InjectView(R.id.input_layout_username)
    private TextInputLayout inputLayoutName;

    @InjectView(R.id.input_login)
    private EditText inputName;

    @InjectView(R.id.input_layout_password)
    private TextInputLayout inputLayoutPassword;

    @InjectView(R.id.input_password)
    private EditText inputPassword;

    @InjectView(R.id.btn_sign_in)
    private Button signIn;

    @Inject
    private SignInPresenter signInPresenter;

    @Inject
    private ViewUtils viewUtils;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        signInPresenter.setView(this);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        signInPresenter.verifyUserLoggedIn();
    }

    /**
     * Validating form
     */
    private void submitForm() {
        String userName = inputName.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        if (!viewUtils.IsValidField(
                userName,
                inputLayoutName,
                inputName,
                getString(R.string.err_msg_name))) {
            return;
        }

        if (!viewUtils.IsValidField(
                password,
                inputLayoutPassword,
                inputPassword,
                getString(R.string.err_msg_password))) {
            return;
        }

        if(!viewUtils.isNetworkAvailable()){
            viewUtils.showToast(getString(R.string.fail_connection));
            return;
        }

        viewUtils.showProgressDialog();
        signInPresenter.signIn(userName, password);
    }

    @Override
    public void doSuccess() {
        viewUtils.hideProgressDialog();
        startMainActivity(MainActivity.USER_NAME, inputName.getText().toString());
    }

    @Override
    public void doFail(String message) {
        viewUtils.showToast(message);
        viewUtils.hideProgressDialog();
    }

    @Override
    public void isLoggedIn() {
        startMainActivity(MainActivity.IS_LOGGED_IN, IS_LOGGED_IN);
    }

    private void startMainActivity(String keyExtra, String valueExtra){
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra(keyExtra, valueExtra);
        startActivity(intent);
        getActivity().finish();
    }
}
