package renan.challange.mobilepayment.domain.database.dao;

import com.google.inject.Singleton;

/**
 * Created by RenanKirk on 05/02/2017.
 */

@Singleton
public class LoginDAO extends AbstractDAO {

    private static final String USER_EMAIL_LOGGED_IN = "user_email_logged_in";

    private static final String USER_NAME_LOGGED_IN = "user_name_logged_in";

    private static final String USER_ID_LOGGED_IN = "user_id_logged_in";

    private static final String API_KEY = "api_key";

    public void setUserEmail(final String email) {
        getEditor().putString(USER_EMAIL_LOGGED_IN, email).commit();
    }

    public String getUserEmail(){
        return getSharedPreferences().getString(USER_EMAIL_LOGGED_IN, "");
    }

    public void setUserName(final String name) {
        getEditor().putString(USER_NAME_LOGGED_IN, name).commit();
    }

    public String getUserName(){
        return getSharedPreferences().getString(USER_NAME_LOGGED_IN, "");
    }

    public void setUserID(final int userId) {
        getEditor().putInt(USER_ID_LOGGED_IN, userId).commit();
    }

    public int getUserID(){
        return getSharedPreferences().getInt(USER_ID_LOGGED_IN, 0);
    }

    public void setApiKey(final String apiKey) {
        getEditor().putString(API_KEY, apiKey).commit();
    }

    public String getApiKey(){
        return getSharedPreferences().getString(API_KEY, "");
    }

}
