package renan.challange.mobilepayment.domain.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import renan.challange.mobilepayment.domain.database.dao.CustomDAO;

/**
 * Created by RenanKirk on 04/02/2017.
 */

@DatabaseTable(tableName = StoreItem.TABLE_NAME_STOREITEM, daoClass = CustomDAO.class)
public class StoreItem {

    public static final String TABLE_NAME_STOREITEM = "items";

    public static final String FIELD_NAME_ID     = "id";
    public static final String FIELD_NAME_NAME   = "name";
    public static final String FIELD_NAME_PRICE   = "price";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_NAME_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_NAME_PRICE)
    private double price;

    public StoreItem(){}

    public StoreItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
