package renan.challange.mobilepayment.domain.bo.impl;

import android.content.Context;

import com.google.inject.Inject;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import renan.challange.mobilepayment.domain.api.PaymentAPI;
import renan.challange.mobilepayment.domain.bo.StoreBO;
import renan.challange.mobilepayment.domain.database.helper.DataBaseHelper;
import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.StorePresenter;
import retrofit2.Call;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class StoreBOImpl extends AbstractBOImpl implements StoreBO {

    private DataBaseHelper dbHelper;

    private Dao<StoreItem, Integer> storeDao;

    @Inject
    private Context context;

    private StorePresenter presenter;

    @Override
    public void createItemOnDB(StoreItem storeItem) throws SQLException {
        storeDao.create(storeItem);
    }

    @Override
    public void setPresenter(StorePresenter presenter) {
        this.presenter = presenter;
        this.dbHelper = OpenHelperManager.getHelper(context, DataBaseHelper.class);
        try {
            this.storeDao = dbHelper.getStoreDao();
        } catch (SQLException e) {
            presenter.failOnCreteDAO(e.getMessage());
        }
    }

    @Override
    public List<StoreItem> getAllStoreItems() throws SQLException {
        return storeDao.queryForAll();
    }

    @Override
    public void buyItem(PaymentRequest item) {

        PaymentAPI api = getRetrofit().create(PaymentAPI.class);

        Call<Void> call = api.postPayment(item);

        call.enqueue(this);
    }

    @Override
    protected void showFail(String message) {
        presenter.buyItemFail(message);
    }

    @Override
    protected void showSuccess(Object body) {
        presenter.buyItemSuccess();
    }
}
