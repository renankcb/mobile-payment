package renan.challange.mobilepayment.domain.api;

import renan.challange.mobilepayment.domain.model.PaymentRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public interface PaymentAPI {

    @Headers({"Content-Type: application/json"})
    @POST("payment")
    Call<Void> postPayment(@Body PaymentRequest paymentRequest);

    @DELETE("payment/{paymentId}")
    Call<Void> deletePayment(@Header("api_key") String api_key, @Path("paymentId") int paymentId);
}
