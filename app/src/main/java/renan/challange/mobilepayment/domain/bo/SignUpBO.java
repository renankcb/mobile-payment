package renan.challange.mobilepayment.domain.bo;

import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.SignUpPresenter;
import renan.challange.mobilepayment.presenter.impl.SignUpPresenterImpl;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface SignUpBO {
    void signUp(User user);

    void setPresenter(SignUpPresenter signUpPresenter);
}
