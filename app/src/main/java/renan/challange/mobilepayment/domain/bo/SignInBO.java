package renan.challange.mobilepayment.domain.bo;

import renan.challange.mobilepayment.presenter.SignInPresenter;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface SignInBO {
    void setPresenter(SignInPresenter presenter);

    void signIn(String userName, String password);
}
