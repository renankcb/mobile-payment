package renan.challange.mobilepayment.domain.bo.impl;

import renan.challange.mobilepayment.domain.api.LoginAPI;
import renan.challange.mobilepayment.domain.bo.SignUpBO;
import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.SignUpPresenter;
import retrofit2.Call;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class SignUpBOImpl extends AbstractBOImpl<Void> implements SignUpBO {

    private SignUpPresenter presenter;

    @Override
    protected void showFail(String message) {
        presenter.doFail(message);
    }

    @Override
    protected void showSuccess(Void body) {
        presenter.doSucess();
    }

    @Override
    public void signUp(User user) {
        LoginAPI api = getRetrofit().create(LoginAPI.class);

        Call<Void> call = api.postUser(user);

        call.enqueue(this);
    }

    @Override
    public void setPresenter(SignUpPresenter signUpPresenter) {
        this.presenter = signUpPresenter;
    }
}
