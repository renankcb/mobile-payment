package renan.challange.mobilepayment.domain.bo;

import renan.challange.mobilepayment.presenter.MainPresenter;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public interface MainBO {
    void getUserByName(String userName);

    void setPresenter(MainPresenter presenter);

    void logout();
}
