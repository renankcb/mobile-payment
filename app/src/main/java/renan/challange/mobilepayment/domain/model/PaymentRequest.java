package renan.challange.mobilepayment.domain.model;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class PaymentRequest {

    private int id;

    private Sender sender;

    private String name;

    private String description;

    private String reference;

    private String date;

    public PaymentRequest(){}

    public PaymentRequest(Sender sender, String name, String description, String reference, String date) {
        this.sender = sender;
        this.name = name;
        this.description = description;
        this.reference = reference;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
