package renan.challange.mobilepayment.domain.api;

import renan.challange.mobilepayment.domain.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public interface UserAPI {

    @GET("users/{username}")
    Call<User> getUser(@Path("username") String user);

    @GET("users/logout")
    Call<Void> userLogout();
}
