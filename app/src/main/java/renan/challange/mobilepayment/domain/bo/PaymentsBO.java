package renan.challange.mobilepayment.domain.bo;

import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import renan.challange.mobilepayment.presenter.impl.PaymentsPresenterImpl;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface PaymentsBO {
    void setPresenter(PaymentsPresenter presenter);

    void deleteItem(int id);
}
