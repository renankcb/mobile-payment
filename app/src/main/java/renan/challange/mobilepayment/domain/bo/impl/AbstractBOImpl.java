package renan.challange.mobilepayment.domain.bo.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public abstract class AbstractBOImpl<T> implements Callback<T>{

    private static final String BASE_URL = "http://192.168.0.106:8080/v1/";

    private Retrofit retrofitInstance;

    protected Retrofit getRetrofit() {
        if (retrofitInstance == null) {
            Gson gson = new GsonBuilder()
                    .create();

            retrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofitInstance;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            showSuccess(response.body());
        } else {
            showFail(response.message());
        }

    }

    protected abstract void showFail(String message);

    protected abstract void showSuccess(T body);

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        showFail(t.getMessage());
    }
}
