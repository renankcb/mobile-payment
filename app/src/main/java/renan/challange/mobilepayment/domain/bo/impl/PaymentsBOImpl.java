package renan.challange.mobilepayment.domain.bo.impl;

import com.google.inject.Inject;

import renan.challange.mobilepayment.domain.api.PaymentAPI;
import renan.challange.mobilepayment.domain.bo.PaymentsBO;
import renan.challange.mobilepayment.domain.database.dao.LoginDAO;
import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import retrofit2.Call;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class PaymentsBOImpl extends AbstractBOImpl<Void> implements PaymentsBO {

    private PaymentsPresenter presenter;

    @Inject
    private LoginDAO session;

    @Override
    public void setPresenter(PaymentsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void deleteItem(int id) {
        PaymentAPI api = getRetrofit().create(PaymentAPI.class);

        Call<Void> call = api.deletePayment(session.getApiKey(), id);

        call.enqueue(this);
    }

    @Override
    protected void showFail(String message) {
        presenter.deletePaymentFail(message);
    }

    @Override
    protected void showSuccess(Void body) {
        presenter.deletePaymentSuccess();
    }
}
