package renan.challange.mobilepayment.domain.bo.impl;

import renan.challange.mobilepayment.domain.api.LoginAPI;
import renan.challange.mobilepayment.domain.bo.SignInBO;
import renan.challange.mobilepayment.presenter.SignInPresenter;
import renan.challange.mobilepayment.presenter.impl.SignInPresenterImpl;
import retrofit2.Call;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class SignInBOImpl extends AbstractBOImpl<Void> implements SignInBO {

    private SignInPresenter presenter;

    @Override
    public void setPresenter(SignInPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void showFail(String message) {
        presenter.doFail(message);
    }

    @Override
    protected void showSuccess(Void body) {
        presenter.doSoccess();
    }

    @Override
    public void signIn(String userName, String password) {
        LoginAPI api = getRetrofit().create(LoginAPI.class);

        Call<Void> call = api.signIn(userName, password);

        call.enqueue(this);
    }
}
