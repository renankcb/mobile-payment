package renan.challange.mobilepayment.domain.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;

import java.sql.SQLException;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class CustomDAO<T, ID> extends BaseDaoImpl<T, ID> {
    protected CustomDAO(final Class<T> dataClass) throws SQLException {
        super(dataClass);
    }

    protected CustomDAO(final ConnectionSource connectionSource, final Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    protected CustomDAO(final ConnectionSource connectionSource, final DatabaseTableConfig<T> tableConfig) throws SQLException {
        super(connectionSource, tableConfig);
    }

    @Override
    public int create(final T data) throws SQLException {
        int result = super.create(data);
        return result;
    }
}
