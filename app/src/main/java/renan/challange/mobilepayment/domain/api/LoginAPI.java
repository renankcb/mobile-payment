package renan.challange.mobilepayment.domain.api;

import renan.challange.mobilepayment.domain.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface LoginAPI {

    @Headers({"Content-Type: application/json"})
    @POST("users")
    Call<Void> postUser(@Body User user);

    @GET("users/login")
    Call<Void> signIn(@Query("username") String userName, @Query("password") String password);
}
