package renan.challange.mobilepayment.domain.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import renan.challange.mobilepayment.domain.model.StoreItem;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME    = "mobilepayment_db.db";
    private static final int    DATABASE_VERSION = 1;

    private Dao<StoreItem, Integer> storeItemDao = null;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, StoreItem.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, StoreItem.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        storeItemDao = null;

        super.close();
    }

    public Dao<StoreItem, Integer> getStoreDao() throws SQLException {
        if (storeItemDao == null) {
            storeItemDao = getDao(StoreItem.class);
        }

        return storeItemDao;
    }
}
