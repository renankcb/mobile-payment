package renan.challange.mobilepayment.domain.bo.impl;

import renan.challange.mobilepayment.domain.api.UserAPI;
import renan.challange.mobilepayment.domain.bo.MainBO;
import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.MainPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public class MainBOImpl extends AbstractBOImpl<User> implements MainBO {

    private MainPresenter presenter;

    @Override
    protected void showFail(String message) {
        presenter.getUserFail(message);
    }

    @Override
    protected void showSuccess(User user) {
        presenter.getUserSuccess(user);
    }

    @Override
    public void getUserByName(String userName) {
        UserAPI api = getRetrofit().create(UserAPI.class);

        Call<User> call = api.getUser(userName);

        call.enqueue(this);
    }

    @Override
    public void setPresenter(MainPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void logout() {
        UserAPI api = getRetrofit().create(UserAPI.class);

        Call<Void> call = api.userLogout();

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                presenter.logoutSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                presenter.logoutFail();
            }
        });
    }
}
