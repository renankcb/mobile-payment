package renan.challange.mobilepayment.domain.bo;

import java.sql.SQLException;
import java.util.List;

import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.StorePresenter;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface StoreBO {
    void createItemOnDB(StoreItem storeItem) throws SQLException;

    void setPresenter(StorePresenter presenter);

    List<StoreItem> getAllStoreItems() throws SQLException;

    void buyItem(PaymentRequest item);
}
