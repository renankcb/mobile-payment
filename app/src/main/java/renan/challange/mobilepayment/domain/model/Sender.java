package renan.challange.mobilepayment.domain.model;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class Sender {

    private String name;

    private String email;

    public Sender(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
