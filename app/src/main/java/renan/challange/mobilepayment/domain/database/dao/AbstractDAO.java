package renan.challange.mobilepayment.domain.database.dao;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.inject.Inject;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class AbstractDAO {

    private static final String PREF_NAME = "PAYMENT_PREFERENCES";

    private static final String SHARED_PREFERENCE = "/shared_prefs/";

    @Inject
    private Context context;

    /**
     * Get the Editor instance for Preferences named PRE_NAME using private
     * mode.
     */
    public SharedPreferences.Editor getEditor() {
        return this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                .edit();
    }

    /**
     * Get Shared Preferences with name PREF_name using MODE_PRIVATE
     */
    public SharedPreferences getSharedPreferences() {
        return this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * clear all preference.
     *
     * @return
     */
    public void clearPreference() {
        getEditor().clear().commit();
    }
}
