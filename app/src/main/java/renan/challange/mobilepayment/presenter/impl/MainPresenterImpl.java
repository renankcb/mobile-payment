package renan.challange.mobilepayment.presenter.impl;

import com.google.inject.Inject;

import renan.challange.mobilepayment.domain.bo.MainBO;
import renan.challange.mobilepayment.domain.database.dao.LoginDAO;
import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.MainPresenter;
import renan.challange.mobilepayment.view.MainView;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public class MainPresenterImpl implements MainPresenter {

    @Inject
    private MainBO mainBO;

    @Inject
    private LoginDAO loginDAO;

    private MainView view;

    @Override
    public void getUserLoggedIn(String userName) {
        mainBO.getUserByName(userName);
    }

    @Override
    public void setView(MainView view) {
        this.view = view;
        mainBO.setPresenter(this);
    }

    @Override
    public void getUserSuccess(User user) {
        loginDAO.setUserEmail(user.getEmail());
        loginDAO.setUserName(user.getUsername());
        loginDAO.setApiKey(user.encriptyApiKey());
        view.getUserSuccess(user);
    }

    @Override
    public void getUserFail(String message) {
        view.getUserFail(message);
    }

    @Override
    public void logout() {
        mainBO.logout();
    }

    @Override
    public void logoutSuccess() {
        loginDAO.clearPreference();
        view.logoutSuccess();
    }

    @Override
    public void logoutFail() {
        view.logoutFail();
    }

    @Override
    public void userLoggedIn() {
        User user = new User(loginDAO.getUserName(), "", "", loginDAO.getUserEmail(), "", "");
        view.getUserSuccess(user);
    }
}
