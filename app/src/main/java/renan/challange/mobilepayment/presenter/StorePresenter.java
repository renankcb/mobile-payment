package renan.challange.mobilepayment.presenter;

import java.sql.SQLException;
import java.util.List;

import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.view.StoreView;
import renan.challange.mobilepayment.view.fragment.StoreFragment;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface StorePresenter {
    void addCreateItemOnDB() throws SQLException;

    void setView(StoreView view);

    List<StoreItem> getAllStoreItems() throws SQLException;

    void failOnCreteDAO(String message);

    void buyItem(StoreItem item);

    void buyItemSuccess();

    void buyItemFail(String message);
}
