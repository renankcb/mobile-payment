package renan.challange.mobilepayment.presenter;

import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.view.PaymentsView;
import renan.challange.mobilepayment.view.fragment.PaymentsListFragment;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface PaymentsPresenter {
    void setView(PaymentsView view);

    void deleteItem(int id);

    void deletePaymentFail(String message);

    void deletePaymentSuccess();
}
