package renan.challange.mobilepayment.presenter.impl;

import com.google.inject.Inject;

import renan.challange.mobilepayment.domain.bo.PaymentsBO;
import renan.challange.mobilepayment.presenter.PaymentsPresenter;
import renan.challange.mobilepayment.view.PaymentsView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class PaymentsPresenterImpl implements PaymentsPresenter {

    private PaymentsView view;

    @Inject
    private PaymentsBO paymentsBO;

    @Override
    public void setView(PaymentsView view) {
        this.view = view;
        paymentsBO.setPresenter(this);
    }

    @Override
    public void deleteItem(int id) {
        paymentsBO.deleteItem(id);
    }

    @Override
    public void deletePaymentFail(String message) {
        view.deletePaymentFail(message);
    }

    @Override
    public void deletePaymentSuccess() {
        view.deletePaymentSuccess();
    }
}
