package renan.challange.mobilepayment.presenter;

import renan.challange.mobilepayment.view.LoginView;
import renan.challange.mobilepayment.view.SignInView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface SignInPresenter{

    /**
     * Setting view to presenter
     * @param view
     */
    void setView(SignInView view);

    /**
     * Used to login user
     * @param userName
     * @param password
     */
    void signIn(String userName, String password);

    /**
     * When return success from BO
     */
    void doSoccess();

    /**
     * When return success from BO
     * @param message
     */
    void doFail(String message);

    void verifyUserLoggedIn();
}
