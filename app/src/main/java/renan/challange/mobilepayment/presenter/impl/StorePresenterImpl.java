package renan.challange.mobilepayment.presenter.impl;

import com.google.inject.Inject;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import renan.challange.mobilepayment.domain.bo.StoreBO;
import renan.challange.mobilepayment.domain.database.dao.LoginDAO;
import renan.challange.mobilepayment.domain.model.PaymentRequest;
import renan.challange.mobilepayment.domain.model.Sender;
import renan.challange.mobilepayment.domain.model.StoreItem;
import renan.challange.mobilepayment.presenter.StorePresenter;
import renan.challange.mobilepayment.view.StoreView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class StorePresenterImpl implements StorePresenter {

    @Inject
    private StoreBO storeBO;

    @Inject
    private LoginDAO session;

    private StoreView view;

    @Override
    public void addCreateItemOnDB() throws SQLException {
        StoreItem storeItem = new StoreItem("Cordão", 10.00);
        storeBO.createItemOnDB(storeItem);
    }

    @Override
    public void setView(StoreView view) {
        this.view = view;
        storeBO.setPresenter(this);
    }

    @Override
    public List<StoreItem> getAllStoreItems() throws SQLException {
        return storeBO.getAllStoreItems();
    }

    @Override
    public void failOnCreteDAO(String message) {
        view.failOnCreteDAO(message);
    }

    @Override
    public void buyItem(StoreItem item) {

        PaymentRequest payment = new PaymentRequest();
        payment.setName(item.getName());
        payment.setDescription(item.getName() + " " + item.getPrice());
        payment.setReference(String.valueOf(item.getId()));
        payment.setDate(new Date().toString());

        Sender sender = new Sender(session.getUserName(), session.getUserEmail());
        payment.setSender(sender);

        storeBO.buyItem(payment);
    }

    @Override
    public void buyItemSuccess() {
        view.buyItemSuccess();
    }

    @Override
    public void buyItemFail(String message) {
        view.buyItemFail(message);
    }
}
