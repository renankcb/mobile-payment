package renan.challange.mobilepayment.presenter;

import renan.challange.mobilepayment.view.LoginView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public interface SignUpPresenter{

    /**
     * Setting view to presenter
     * @param view
     */
    void setView(LoginView view);

    /**
     * Used to create user
     * @param name
     * @param fName
     * @param lName
     * @param mail
     * @param pass
     * @param pNumber
     */
    void signUup(String name, String fName, String lName, String mail, String pass, String pNumber);

    /**
     * when return success from BO
     */
    void doSucess();

    /**
     * when return fail from BO
     * @param message
     */
    void doFail(String message);
}
