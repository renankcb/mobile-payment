package renan.challange.mobilepayment.presenter.impl;

import com.google.inject.Inject;

import renan.challange.mobilepayment.domain.bo.SignInBO;
import renan.challange.mobilepayment.domain.database.dao.LoginDAO;
import renan.challange.mobilepayment.presenter.SignInPresenter;
import renan.challange.mobilepayment.view.LoginView;
import renan.challange.mobilepayment.view.SignInView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class SignInPresenterImpl implements SignInPresenter {

    private SignInView view;

    @Inject
    private SignInBO signInBO;

    @Inject
    private LoginDAO login;

    @Override
    public void setView(SignInView view) {
        this.view = view;
        signInBO.setPresenter(this);
    }

    @Override
    public void signIn(String userName, String password) {
        signInBO.signIn(userName, password);
    }

    @Override
    public void doSoccess() {
        view.doSuccess();
    }

    @Override
    public void doFail(String message) {
        view.doFail(message);
    }

    @Override
    public void verifyUserLoggedIn() {
        if(!login.getUserName().isEmpty()){
            view.isLoggedIn();
        }
    }
}
