package renan.challange.mobilepayment.presenter;

import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.view.MainView;
import renan.challange.mobilepayment.view.activity.MainActivity;

/**
 * Created by RenanKirk on 05/02/2017.
 */

public interface MainPresenter {
    void getUserLoggedIn(String name);

    void setView(MainView view);

    void getUserSuccess(User user);

    void getUserFail(String message);

    void logout();

    void logoutSuccess();

    void logoutFail();

    void userLoggedIn();
}
