package renan.challange.mobilepayment.presenter.impl;

import com.google.inject.Inject;

import renan.challange.mobilepayment.domain.bo.SignUpBO;
import renan.challange.mobilepayment.domain.model.User;
import renan.challange.mobilepayment.presenter.SignUpPresenter;
import renan.challange.mobilepayment.view.LoginView;

/**
 * Created by RenanKirk on 04/02/2017.
 */

public class SignUpPresenterImpl implements SignUpPresenter {

    @Inject
    private SignUpBO signUpBO;

    private LoginView view;

    @Override
    public void setView(LoginView view) {
        this.view = view;
        signUpBO.setPresenter(this);
    }

    @Override
    public void signUup(String name, String fName, String lName, String mail, String pass, String pNumber) {
        User user = new User(name, fName, lName, mail, pass, pNumber);
        signUpBO.signUp(user);
    }

    @Override
    public void doSucess() {
        view.doSuccess();
    }

    @Override
    public void doFail(String message) {
        view.doFail(message);
    }
}
