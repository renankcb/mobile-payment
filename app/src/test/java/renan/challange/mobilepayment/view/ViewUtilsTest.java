package renan.challange.mobilepayment.view;

import android.app.Activity;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import renan.challange.mobilepayment.view.Utils.ViewUtils;

/**
 * Created by RenanKirk on 04/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ViewUtilsTest {

    private ViewUtils viewUtils;

    @Mock
    Activity mMockContext;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        viewUtils = new ViewUtils(mMockContext);
    }

    @Test
    public void validateFieldReturnFalseWhenStringEmpty(){
        //given
        String field = "";

        //then
        Assert.assertFalse(this.viewUtils.isValid(field));
    }

    @Test
    public void validateFieldReturnTrueWhenStringNoEmpty(){
        //given
        String field = "payment";

        //then
        Assert.assertTrue(this.viewUtils.isValid(field));
    }

}
