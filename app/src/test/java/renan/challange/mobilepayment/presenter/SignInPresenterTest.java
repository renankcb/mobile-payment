package renan.challange.mobilepayment.presenter;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import renan.challange.mobilepayment.domain.bo.SignInBO;
import renan.challange.mobilepayment.domain.database.dao.LoginDAO;
import renan.challange.mobilepayment.presenter.impl.SignInPresenterImpl;
import renan.challange.mobilepayment.view.SignInView;

/**
 * Created by RenanKirk on 07/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SignInPresenterTest {

    private SignInPresenter presenter;

    @Mock
    private LoginDAO loginDAO;

    @Mock
    private SignInView view;

    @Before
    public void init(){
        presenter = new SignInPresenterImpl();

        Whitebox.setInternalState(presenter, "login", loginDAO);

        Whitebox.setInternalState(presenter, "view", view);
    }

    @Test
    public void verifyUserLoggedInTrue(){
        Mockito.when(loginDAO.getUserName()).thenReturn("asd");

        presenter.verifyUserLoggedIn();

        Mockito.verify(view, Mockito.times(1)).isLoggedIn();
    }

    @Test
    public void verifyUserLoggedInFalse(){
        Mockito.when(loginDAO.getUserName()).thenReturn("");

        presenter.verifyUserLoggedIn();

        Mockito.verify(view, Mockito.times(0)).isLoggedIn();
    }

}
