package renan.challange.mobilepayment;

import android.support.annotation.UiThread;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import renan.challange.mobilepayment.view.activity.LoginActivity;
import renan.challange.mobilepayment.view.fragment.SignInFragment;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isFocusable;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by RenanKirk on 05/02/2017.
 */

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {
    private LoginActivity loginActivity;

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void setUp() {
        loginActivity = mActivityRule.getActivity();
        assertThat(loginActivity, notNullValue());
    }

    @Test
    public void swipePage() {
        onView(withId(R.id.viewpager_login))
                .check(matches(isDisplayed()));

        onView(withId(R.id.viewpager_login))
                .perform(swipeLeft());
    }

    @Test
    public void checkTabLayoutDisplayed() {
        onView(withId(R.id.tabs_login))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    @UiThread
    public void showErrorWhenEditTextHasNoValue(){
        onView(withText(SignInFragment.SIGNIN_TAB_TITLE))
                .perform((click()))
                .check(matches(isDisplayed()));

        onView(allOf(withId(R.id.input_login), isCompletelyDisplayed()))
                .perform(typeText("dsa"));

        onView(allOf(withId(R.id.input_login), isCompletelyDisplayed()))
                .perform(click());

        onView(allOf(withId(R.id.input_password), isCompletelyDisplayed()))
                .check(matches(isFocusable()));
    }
}
